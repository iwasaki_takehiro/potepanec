module CategoriesHelper
  def get_belongs_to_taxon(product)
    if product.taxons.present?
      product.taxons.first.id
    else
      Spree::Taxonomy.first.id
    end
  end
end
