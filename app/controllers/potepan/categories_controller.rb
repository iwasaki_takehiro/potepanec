module Potepan
  class CategoriesController < ApplicationController
    def show
      @taxon = Spree::Taxon.find(params[:id])
      @taxonomies = Spree::Taxon.roots
      @products = @taxon.all_products.includes(master: [:images, :default_price])
    end
  end
end
