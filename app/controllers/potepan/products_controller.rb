module Potepan
  class ProductsController < ApplicationController
    ASSOCIATED_PRODUCTS_SET = 4
    def show
      @product = Spree::Product.find(params[:id])
      @associated_products = @product.associated_products.includes(master: [:images, :default_price]).sample(ASSOCIATED_PRODUCTS_SET)
    end
  end
end
