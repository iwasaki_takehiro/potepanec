module Potepan::ProductDecorator
  def associated_products
    if taxons.present?
      Spree::Product.in_taxons(taxons).where.not(id: id).distinct
    else
      Spree::Product.none
    end
  end
  Spree::Product.prepend self
end
