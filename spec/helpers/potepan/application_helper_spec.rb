require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'full title helper' do
    it 'title expect to be only base_title when argument is empty' do
      expect(full_title('')).to eq 'BIGBAG Store'
    end

    it 'title expect to be only base_title when argument is nil' do
      expect(full_title(nil)).to eq 'BIGBAG Store'
    end

    it 'title expect to be with page_title when argument exists' do
      expect(full_title('foobar')).to eq 'foobar - BIGBAG Store'
    end
  end
end
