require 'rails_helper'

RSpec.describe CategoriesHelper, type: :helper do
  let(:product) { create :product, taxons: [taxon] }
  let(:product1) { create :product }
  let!(:taxonomy) { create :taxonomy, name: "Categories" }
  let(:taxon) { create :taxon }

  describe 'helper get_belongs_to_taxon' do
    context 'when the product belongs to taxon' do
      it 'expect routes is correct' do
        potepan_category_path(get_belongs_to_taxon(product))
        expect(response).to have_http_status(200)
      end
    end

    context 'when the product dose not belongs to taxon' do
      it 'expect routes is correct' do
        potepan_category_path(get_belongs_to_taxon(product1))
        expect(response).to have_http_status(200)
      end
    end
  end
end
