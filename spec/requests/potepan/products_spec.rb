require 'rails_helper'

RSpec.describe 'templates', type: :request do
  let(:product) { create :product, taxons: [taxon, taxon1] }
  let(:product1) { create :product }
  let!(:product2) { create :product, taxons: [taxon, taxon1] }
  let!(:products) { create_list :product, 5, taxons: [taxon] }
  let!(:image) { create :image, viewable_id: product.id, viewable_type: "Spree::Variant" }
  let!(:image2) { create :image, viewable_id: product2.id, viewable_type: "Spree::Variant" }
  let(:taxonomy) { create :taxonomy, name: "Categories" }
  let(:taxon) { taxonomy.root.descendants.create name: "taxon" }
  let(:taxon1) { taxonomy.root.descendants.create name: "taxon1" }

  describe 'potepan_product_path' do
    before do
      get potepan_product_path(product.id)
    end

    it 'expect routes is correct' do
      expect(response).to have_http_status(200)
    end

    it 'expect show name' do
      expect(response.body).to include product.name
    end

    it 'expect show description' do
      expect(response.body).to include product.description
    end

    it 'expect show correct price' do
      expect(response.body).to include product.display_price.to_s
    end

    it 'expect show images' do
      expect(product.images).to be_exist
      product.images.each do |image|
        expect(response.body).to include image.attachment(:large)
        expect(response.body).to include image.attachment(:small)
      end
    end

    it 'expect has categories path' do
      expect(response.body).to include potepan_category_path(get_belongs_to_taxon(product))
    end

    context 'potepan_product_path without taxons' do
      before do
        get potepan_product_path(product1.id)
      end

      it 'expect do not show partial of associated_products' do
        expect(response.body).not_to include "関連商品"
      end
    end
  end
end
