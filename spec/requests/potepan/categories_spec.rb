require 'rails_helper'

RSpec.describe 'potepan_categories_path', type: :request do
  let(:product) { create :product, taxons: [taxon] }
  let(:product1) { create :product, taxons: [taxon1] }
  let!(:image) { create :image, viewable_id: product.id, viewable_type: "Spree::Variant" }
  let!(:image1) { create :image, viewable_id: product1.id, viewable_type: "Spree::Variant" }
  let(:taxonomy) { create :taxonomy }
  let(:taxon) { taxonomy.root.descendants.create name: "taxon", parent_id: taxonomy.root.id }
  let(:taxon1) { taxonomy.root.descendants.create name: "taxon1", parent_id: taxonomy.root.id }

  context 'when GET potepan_categories_path(taxonomy)' do
    before do
      get potepan_category_path(taxonomy.root.id)
    end

    it 'expect routes is correct(categories)' do
      expect(response).to have_http_status(200)
    end

    it 'expect show taxon name' do
      expect(response.body).to include taxon.name
    end

    it 'expect show proudcts count' do
      expect(response.body).to include taxon.products.count.to_s
    end

    it 'expect show all images' do
      expect(response.body).to include product.display_image.attachment(:large)
      expect(response.body).to include product1.display_image.attachment(:large)
    end
  end

  context 'when GET potepan_categories_path(taxon)' do
    before do
      get potepan_category_path(taxon.id)
    end

    it 'expect routes is correct(not root)' do
      expect(response).to have_http_status(200)
    end

    it 'expect show taxon name' do
      expect(response.body).to include taxon.name
    end

    it 'expect show proudcts count' do
      expect(response.body).to include taxon.products.count.to_s
    end

    it 'expect show only one images' do
      expect(response.body).to include product.display_image.attachment(:large)
      expect(response.body).not_to include product1.display_image.attachment(:large)
    end
  end
end
