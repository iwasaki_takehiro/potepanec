require 'rails_helper'

RSpec.describe 'ProductDecorator', type: :model do
  let(:taxonomy) { create :taxonomy }
  let(:taxon) { create :taxon }
  let(:taxon1) { create :taxon }
  let(:taxon2) { create :taxon }
  let(:main_product) { create :product, taxons: [taxon, taxon1] }
  let!(:associated_product) { create :product, taxons: [taxon, taxon1] }
  let!(:unassociated_product_with_taxon) { create :product, taxons: [taxon2] }
  let!(:unassociated_product_without_taxon) { create :product }

  describe 'associated_products' do
    it 'expect array has associatied product' do
      expect(main_product.associated_products).to match_array([associated_product])
    end
  end
end
